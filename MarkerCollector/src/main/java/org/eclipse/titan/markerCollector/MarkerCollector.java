/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.markerCollector;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

public class MarkerCollector {
	public static class CommandLineArgs {
		public String compilerPath;
		public String testRoot;
		
		public boolean oopEnabled;
		public boolean files;
		
		public List<String> filenames = new ArrayList<>();
		public List<String> excludedFilenames = new ArrayList<>();
	}
	
	boolean comma;
	String filename;
	String testRoot;
	CommandLineArgs args;
	Path rootPath;
	
	final static Map<String, List<MarkerToCheck>> testMarkers = new HashMap<>();
	
	public void execute(final CommandLineArgs args) {
		this.args = args;
		if (!args.files && args.testRoot == null) {
			System.err.println("Missing test root");
			System.exit(1);
		}
		testRoot = args.testRoot;
		rootPath = Path.of(testRoot);
		
		List<String> builderList = new ArrayList<>(
			List.of(
				args.compilerPath + File.separatorChar + "compiler", "-s"
			)
		);
		if (args.oopEnabled) {
			builderList.add("-k");
		}
		
		File tempfile = null;
		BufferedWriter out = null;
		try {
			tempfile = File.createTempFile("temp", null);
			tempfile.deleteOnExit();
			FileWriter fw = new FileWriter(tempfile);
			out = new BufferedWriter(fw);
			List<String> filelist = args.files ? args.filenames : findSourceFiles(args.testRoot);
			for (String file : filelist) {
				out.write(file);
				out.newLine();
			}
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}			

		builderList.add("-J");
		builderList.add(tempfile.getAbsolutePath());
		System.out.println("\tprivate List<String> FUNCTION_NAME() {\n"
				+ "\t\treturn\n"
				+ "\t\t\tList.of(");
		ProcessBuilder builder = new ProcessBuilder(builderList);
		try {
			builder.redirectErrorStream(true);
			Process process = builder.start();
			InStream stream = new InStream(process.getInputStream(), this::ConsumeInput);
			ExecutorService executor = Executors.newSingleThreadExecutor();
			Future<?> future = executor.submit(stream);
	
			/** 
			 * FIXME :: Titan core compiler exit code can be other than 0 even for successful parse and semantic analysis
			 */
			int code =	process.waitFor();
//			if (code != 0) {
//				System.out.println("Check failed");
//				System.exit(-1);
//			}
			executor.shutdown();
			future.get();
			System.out.println("\n\t\t);\n\t}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public class InStream implements Runnable {
	    private InputStream inputStream;
	    private Consumer<String> consumer;

	    public InStream(InputStream inputStream, Consumer<String> consumer) {
	        this.inputStream = inputStream;
	        this.consumer = consumer;
	    }

	    @Override
	    public void run() {
	        new BufferedReader(new InputStreamReader(inputStream)).lines().forEach(consumer);
	    }
	}
	
	private void ConsumeInput(String str) {
		Pattern pattern = Pattern.compile("\\s*(.*):(\\d+)\\.\\d+-\\d+:\\s+(\\w+):\\s*(.+)");
		Matcher matcher = pattern.matcher(str);
		if (!matcher.find()) {
			return;
		}
		
		String filename = matcher.group(1); 
		int line = Integer.parseInt(matcher.group(2));
		String severity = matcher.group(3);
		String message = matcher.group(4).replace("\"", "\\\"").replace("\\", "\\\\");
		
		switch (severity) {
		case "error":
		default:
			severity = "E";
			break;
		case "warning":
			severity = "W";
			break;
		}
		
		Path filePath = Path.of(filename);
		Path shortPath = rootPath.relativize(filePath);
		
		if (!comma) {
			comma = true;
		} else {
			System.out.println(",");
		}
		System.out.print("\t\t\t\t\"" + shortPath.toString().replace("\\", "\\\\") + ":::" + line + ":::" + severity + ":::" + message.trim() + "\"");
	}
	
	private List<String> findSourceFiles(String testRoot) {
		List<String> files = new ArrayList<>();
		Path testPath = Path.of(testRoot);
		try {
			Files.walkFileTree(testPath, new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					final String filename = file.getFileName().toString();
					if (isSupportedExtension(filename) && !args.excludedFilenames.contains(filename)) {
						synchronized (files) {
							files.add(file.toString());
						}
					}
					return FileVisitResult.CONTINUE;
				}
				
				 @Override
			        public FileVisitResult visitFileFailed(Path file, IOException exc) {
			            return FileVisitResult.CONTINUE;
			        }
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return files;
	}
	
	private boolean isSupportedExtension(final String filename) {
		if (!filename.contains(".")) {
			return false;
		}
		final String extension = filename.substring(filename.lastIndexOf(".") + 1);
		switch (extension) {
		case "ttcn":
		case "ttcn3":
		case "ttcnpp":
		case "asn":
		case "asn1":
		case "cfg":
			return true;
		default:
			break;
		}
		
		return false;
	}
}
