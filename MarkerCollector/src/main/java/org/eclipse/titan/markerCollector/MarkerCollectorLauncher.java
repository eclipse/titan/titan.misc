/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.markerCollector;

import java.util.Arrays;

import org.eclipse.titan.markerCollector.MarkerCollector.CommandLineArgs;

public final class MarkerCollectorLauncher {
	private static final String helpText = "Usage: ...";

	public static void main(String[] args) {
		if (args.length < 1) {
			System.out.println("Please provide your inputs!\n" + helpText);
		}
		
		final MarkerCollector collector = new MarkerCollector();
		final CommandLineArgs cmdargs = new MarkerCollector.CommandLineArgs();

		for (int i = 0; i < args.length; i++) {
			if (args[i].startsWith("--")) {
				switch (args[i]) {
				case "--compiler":
					if (i + 1 == args.length) {
						System.err.println("Missing compiler path");
						System.exit(-1);
					}
					cmdargs.compilerPath = args[++i];
					break;
				case "--oop":
					cmdargs.oopEnabled = true;
					break;
				case "--files":
					cmdargs.files = true;
					break;
				case "--exclude":
					cmdargs.excludedFilenames = Arrays.asList(args[++i].split(";"));
					break;
				}
			} else {
				if (cmdargs.files ) {
					cmdargs.filenames.add(args[i]);
				} else {
					cmdargs.testRoot = args[i];
				}
			}
		}
		
		collector.execute(cmdargs);
	}

}
