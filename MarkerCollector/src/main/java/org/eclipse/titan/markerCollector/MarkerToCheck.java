/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.markerCollector;

/**
 * @author Miklos Magyari
 */
public class MarkerToCheck {
	public enum MarkerSeverity {
		Error,
		Warning,
		Any
	}
	
	private String messageToCheck;
	private int lineNumber;
	private MarkerSeverity messageSeverity; 
	
	public MarkerToCheck(final String messageToCheck, final int lineNumber, final MarkerSeverity messageSeverity)
	{
		this.messageToCheck = messageToCheck;
		this.lineNumber = lineNumber;
		this.messageSeverity = messageSeverity;
	}

	public MarkerSeverity getMessageSeverity() {
		return messageSeverity;
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public String getMessageToCheck() {
		return messageToCheck;
	}
	
	@Override
	public boolean equals(Object other) {
		if (other instanceof MarkerToCheck) {
			MarkerToCheck omarker = (MarkerToCheck)other;
			// FIXME :: severity
			return lineNumber == omarker.lineNumber && messageToCheck.equals(omarker.messageToCheck);
		}
		return false;
	}
}
