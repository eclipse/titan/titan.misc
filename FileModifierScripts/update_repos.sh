#!/bin/bash -x
###############################################################################
# Copyright (c) 2000-2025 Ericsson Telecom AB
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
###############################################################################
#
# Updates the git repos related to the Eclipse Titan project
# Usage:
#   1. clone the titan.misc project in the folder where you want to
# 	   clone/pull the titan repos
#   2. copy this script to the that folder
#   3. run this script
#
TITAN_PREFIX=titan.
ECLIPSE_GITLAB_PREFIX=git@gitlab.eclipse.org:eclipse/titan/
GIT_SUFFIX=.git
COPYRIGHT_UPDATE_SCRIPT=change_copyright6.pl
COPYRIGHT_UPDATE_SCRIPT_PATH=titan.misc/FileModifierScripts
LEGAL_DOC_TEMPLATES_PATH=${COPYRIGHT_UPDATE_SCRIPT_PATH}/templates

REPOS=(ApplicationLibraries.CoAP \
	ApplicationLibraries.HTTP \
	ApplicationLibraries.LWM2M \
	ApplicationLibraries.MBT \
	ApplicationLibraries.MQTT \
	Applications.IoT_Functiontest_Framework \
	Applications.IoT_Loadtest_Framework \
	Applications.RIoT \
	Libraries.CLL \
	Libraries.ServiceFramework \
	Libraries.TCCUsefulFunctions \
	Libraries.TLS \
	Libraries.Web_GUI \
	ProtocolEmulations.M3UA \
	ProtocolEmulations.SCCP \
	ProtocolEmulations.SUA \
	ProtocolEmulations.SCTP \
	ProtocolModules.5G_system_TS29502_Nsmf_v15 \
	ProtocolModules.5G_system_TS29503_Nudm_v15 \
	ProtocolModules.5G_system_TS29508_Nsmf_v15 \
	ProtocolModules.5G_system_TS29509_Nausf_v15 \
	ProtocolModules.5G_system_TS29510_Nnrf_v15 \
	ProtocolModules.5G_system_TS29511_N5g_eir_v15 \
	ProtocolModules.5G_system_TS29512_Npcf_v15 \
	ProtocolModules.5G_system_TS29514_Npcf_v15 \
	ProtocolModules.5G_system_TS29518_Namf_v15 \
	ProtocolModules.5G_system_TS29520_Nnwdaf_v15 \
	ProtocolModules.5G_system_TS29571_CommonData_v15 \
	ProtocolModules.5G_system_TS29572_Nlmf_v15 \
	ProtocolModules.5G_system_TS29594_Nchf_v15 \
	ProtocolModules.5G_system_TS32291_Nchf_v15 \
	ProtocolModules.BSSAPP_v7.3.0 \
	ProtocolModules.BSSGP_v13.0.0 \
	ProtocolModules.BSSMAP_v11.2.0 \
	ProtocolModules.CoAP \
	ProtocolModules.COMMON \
	ProtocolModules.DHCP \
	ProtocolModules.DHCPv6 \
	ProtocolModules.DIAMETER_ProtocolModule_Generator \
	ProtocolModules.DNS \
	ProtocolModules.DSS1_ETSI \
	ProtocolModules.DUA \
	ProtocolModules.EAP \
	ProtocolModules.FrameRelay \
	ProtocolModules.GCP_31r1 \
	ProtocolModules.GRE \
	ProtocolModules.GTP_v13.5.0 \
	ProtocolModules.GTPv2_v13.7.0 \
	ProtocolModules.GTPv2_v15.2.0 \
	ProtocolModules.H248_v2 \
	ProtocolModules.HTTP2 \
	ProtocolModules.HTTP2.0 \
	ProtocolModules.ICAP \
	ProtocolModules.ICMP \
	ProtocolModules.ICMPv6 \
	ProtocolModules.IKEv2 \
	ProtocolModules.IMAP_4rev1 \
	ProtocolModules.IP \
	ProtocolModules.IPsec \
	ProtocolModules.ISUP_Q.762 \
	ProtocolModules.IUA \
	ProtocolModules.JSON_v07_2006 \
	ProtocolModules.JSON_Web_Signature \
	ProtocolModules.L2TP \
	ProtocolModules.LLC_v7.1.0 \
	ProtocolModules.M2PA \
	ProtocolModules.M2UA \
	ProtocolModules.M3UA \
	ProtocolModules.MIME \
	ProtocolModules.MobileL3_v13.4.0 \
	ProtocolModules.MongoDB \
	ProtocolModules.MSRP \
	ProtocolModules.MQTT \
	ProtocolModules.NAS_EPS_15.2.0.1 \
	ProtocolModules.NDP \
	ProtocolModules.NS_v7.3.0 \
	ProtocolModules.NTAF \
	ProtocolModules.OPC_UA \
	ProtocolModules.PFCP_v15.1.0 \
	ProtocolModules.PPP \
	ProtocolModules.ProtoBuff \
	ProtocolModules.RADIUS_ProtocolModule_Generator \
	ProtocolModules.ROSE \
	ProtocolModules.RTP \
	ProtocolModules.RTSP \
	ProtocolModules.SCTP \
	ProtocolModules.SDP \
	ProtocolModules.SGsAP_13.2.0 \
	ProtocolModules.SMPP \
	ProtocolModules.SMTP \
	ProtocolModules.SNDCP_v7.0.0 \
	ProtocolModules.SNMP \
	ProtocolModules.SRTP \
	ProtocolModules.STOMP \
	ProtocolModules.STUN \
	ProtocolModules.STUN_RFC5389 \
	ProtocolModules.SUA \
	ProtocolModules.TCP \
	ProtocolModules.TLS \
	ProtocolModules.UDP \
	ProtocolModules.V5 \
	ProtocolModules.WebSocket \
	ProtocolModules.WTP \
	ProtocolModules.XML_RPC \
	ProtocolModules.XMPP \
	ProtocolModules.XTDP \
	Servers.GTP_Tunnel_Daemon \
	Servers.IP_Daemon_Dynamic \
	Servers.SCTP_Daemon_Dynamic \
	TestPorts.Common_Components.Abstract_Socket \
	TestPorts.Common_Components.Socket-API \
	TestPorts.GPIO \
	TestPorts.HTTPmsg \
	TestPorts.IPL4asp \
	TestPorts.LANL2asp \
	TestPorts.LDAPasp_RFC4511 \
	TestPorts.LDAPmsg \
	TestPorts.MTP3asp \
	TestPorts.PCAPasp \
	TestPorts.PIPEasp \
	TestPorts.SCTPasp \
	TestPorts.Serial \
	TestPorts.SIPmsg \
	TestPorts.SocketCANasp \
	TestPorts.SQLasp \
	TestPorts.SSHCLIENTasp \
	TestPorts.STDINOUTmsg \
	TestPorts.SUNRPCasp \
	TestPorts.TCPasp \
	TestPorts.TELNETasp \
	TestPorts.Thrift_TPG \
	TestPorts.UDPasp \
	TestPorts.UNIX_DOMAIN_SOCKETasp)
	#common-tests \
	#core \
	#EclipsePlug-ins \
	#language-server \
	#misc \
	#ObjectOriented.StandardCollections \
	#vs-code-extension)

# for testing
#REPOS=(ObjectOriented.StandardCollections)

init_repo () {
	if [ -d $1 ]; then
		cd $1
		git pull
		cd ..
	else
		git clone ${ECLIPSE_GITLAB_PREFIX}$1${GIT_SUFFIX}
	fi
	if [ ! -d $1 ]; then
		echo "Missing repository: $1"
		exit 1
	fi
}

copy_legal_files () {
	if [ ! -d ${LEGAL_DOC_TEMPLATES_PATH} ]; then
		echo "Directory not found: ${LEGAL_DOC_TEMPLATES_PATH}"
		exit 1
	fi
	cp ${LEGAL_DOC_TEMPLATES_PATH}/*.md $1
	sed -i "s/repo_name/$1/" $1/README.md
	
	cd $1
	git checkout -b legal
	git add --all
	git commit -a -s -m "Adding legal docs"
	cd ..
}

update_copyright () {
	if [ ! -d ${COPYRIGHT_UPDATE_SCRIPT_PATH} ]; then
		echo "Directory not found: ${COPYRIGHT_UPDATE_SCRIPT_PATH}"
		exit 1
	fi
	cp ${COPYRIGHT_UPDATE_SCRIPT_PATH}/${COPYRIGHT_UPDATE_SCRIPT} $1
	cd $1
	perl ${COPYRIGHT_UPDATE_SCRIPT}
	rm ${COPYRIGHT_UPDATE_SCRIPT}
	
	git commit -a -s -m "Updating copyright"
	cd ..
}

for repo in ${REPOS[@]}; do
	repo=${TITAN_PREFIX}${repo}
	
	init_repo ${repo}
	
	#copy_legal_files ${repo}
	
	#update_copyright ${repo}
	
	cd ${repo}
	sed -i "s/https\:\/\/github\.com\/eclipse\/titan\.core/https\:\/\/gitlab\.eclipse\.org\/eclipse\/titan\/titan\.core/" README.md
	git commit -a -s -m "Link fix in readme"
	git push
	cd ..
done